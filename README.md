---
marp: true
_class: invert
---

# Python Web

Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com

---
<!-- _class: invert -->

# HTTP

---

HTTP - HyperText Transfer Protocol

- HTML - HyperText Markup Language
- TCP/IP - Protocolo Cliente Servidor
- [Requisições](https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods):
    GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH
- [Respostas](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status):
    [OK - 200](https://http.cat/200)
    [Created - 201](https://http.cat/201)
    [Bad Request - 400](https://http.cat/400)
    [Not Found - 404](https://http.cat/404)
    [I'm a teapot - 418](https://http.cat/418)
    [Server Error - 500](https://http.cat/500)

---

http://pudim.com.br

```sh
$ ping pudim.com.br
PING pudim.com.br (54.207.20.104) 56(84) bytes of data.
```

```sh
$ telnet 54.207.20.104 80
GET / HTTP/1.1
Host: pudim.com.br

GET / HTTP/1.1
Host: pudim.com.br
...
```


---
<!-- paginate: true -->
<!-- _class: invert -->

# WSGI

<!-- _footer: https://wsgi.readthedocs.io/en/latest/what.html -->

---

WSGI - Web Server Gateway Interface

```python
def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    return [b"Hello World"]
```

---

<!-- _class: invert -->

# ASGI

<!-- _footer: https://asgi.readthedocs.io/en/latest/ -->

---

ASGI - Asynchronous Server Gateway Interface


```python
async def application(scope, receive, send):
    assert scope['type'] == 'http'

    await send({
        'type': 'http.response.start',
        'status': 200,
        'headers': [
            [b'content-type', b'text/plain'],
        ],
    })
    await send({
        'type': 'http.response.body',
        'body': b'Hello, world!',
    })
```


---

<!-- _class: invert -->

# Django

The web framework for perfectionists with deadlines

<!-- _footer: https://www.djangoproject.com/ -->

---


- Instalação

    [pypi](https://pypi.org/)
    [pipenv](https://github.com/pypa/pipenv)

- Tutorial

    https://docs.djangoproject.com/en/3.2/intro/tutorial01/
    https://docs.djangoproject.com/pt-br/3.2/intro/tutorial01/

---

<!-- paginate: false -->
<!-- _class: invert -->

# Obrigado!
Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com
